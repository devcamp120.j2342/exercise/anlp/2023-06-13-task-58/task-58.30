package com.devcamp.task5830.read_list_customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5830.read_list_customer.model.CCustomer;

public interface CCustomerRepository extends JpaRepository<CCustomer, Long>{
    
}
